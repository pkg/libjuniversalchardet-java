libjuniversalchardet-java (2.4.0-3+apertis1) apertis; urgency=medium

  * Move package to development repository. Needed for the Java suite

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 10 Oct 2023 21:51:42 +0530

libjuniversalchardet-java (2.4.0-3+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 05 Oct 2023 16:56:35 +0000

libjuniversalchardet-java (2.4.0-3) unstable; urgency=medium

  * Team upload
  * Remove Thomas Koch from Uploaders (Closes: #1019026)
  * Bump Standards-Version to 4.6.1
  * Set Rules-Requires-Root: no in debian/control
  * Freshen years in debian/copyright
  * Mark package Multi-Arch: foreign

 -- tony mancill <tmancill@debian.org>  Sat, 03 Sep 2022 08:30:06 -0700

libjuniversalchardet-java (2.4.0-2) unstable; urgency=medium

  * Upload to unstable.
  * Remove maven.publishedRules in favor of relocation.
  * Relocate the old groupId to fix FTBFS with reverse-dependencies.
  * Declare compliance with Debian Policy 4.6.0.

 -- Markus Koschany <apo@debian.org>  Thu, 19 Aug 2021 12:47:36 +0200

libjuniversalchardet-java (2.4.0-1) experimental; urgency=medium

  * New upstream version 2.4.0.
  * Add myself to Uploaders.
  * wrap-and-sort -sa.
  * Declare compliance with Debian Policy 4.5.1.
  * Drop the javadoc documentation.
  * Remove orig-tar.sh script
  * Remove README.source.
  * Switch to debhelper-compat = 13.
  * Switch to dh sequencer.
  * Drop remove_odd_test.patch
  * Update debian/watch and point to new source repository.
  * Update package description and remove some outdated information.
  * Use canonical VCS URL and update the homepage.

 -- Markus Koschany <apo@debian.org>  Tue, 06 Apr 2021 11:40:23 +0200

libjuniversalchardet-java (1.0.3-1.1+apertis1) apertis; urgency=medium

  * Set component to sdk. Move java packages to sdk to avoid building
    for arm architecture.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 22 Feb 2022 17:22:27 +0530

libjuniversalchardet-java (1.0.3-1.1+apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 19 May 2021 17:42:39 -0300

libjuniversalchardet-java (1.0.3-1) unstable; urgency=low

  * Initial release. (Closes: #685873)

 -- Thomas Koch <thomas@koch.ro>  Tue, 18 Jun 2013 14:22:51 +0200
